/*===========================================================================================

     MrDIY Geolocation - Find Me ( without GPS)

     Getting the API: https://developers.google.com/maps/documentation/geolocation/intro

     Watch the video - https://youtu.be/nHaEYJzduZM

  =========================================================================================== */

#include <ESP8266WiFi.h>
#include <TimeLib.h>
#include "cert.h"

#define WIFI_NAME   "ssid"
#define WIFI_PASS   "password"
#define API_KEY     "api_key"

const char*         host = "www.googleapis.com";
const String        url = "/geolocation/v1/geolocate?key=";


/* ############################ setup ########################################### */

void setup() {

  Serial.begin(115200);
  Serial.println(); Serial.println(); Serial.println();
  Serial.println("======================================================================");
  Serial.println("                         MrDIY Geolocation");
  Serial.println("======================================================================\n");

  get_my_location();

}


void loop() {}


void get_my_location() {


  // Searching for Wifis -----------------------------------------------------------
  int n = WiFi.scanNetworks();

  Serial.print("------ Scan (");  Serial.print(n);  Serial.println(")-------");

  // Listing Wifis found ----------------------------------------------------------
  for (int j = 0; j < n; ++j) {
    Serial.println( WiFi.BSSIDstr(j) );
  }



  // Preparing Json needed for API call -------------------------------------------

  String jsonString = "{ \"considerIp\": \"false\", \"wifiAccessPoints\": [";
  for (int j = 0; j < n; ++j) {
    jsonString += "{";
    jsonString += "\"macAddress\":\"";
    jsonString += (WiFi.BSSIDstr(j));
    jsonString += "\",";
    jsonString += "\"signalStrength\":";
    jsonString += WiFi.RSSI(j);
    jsonString += ",";
    jsonString += "\"channel\":";
    jsonString += WiFi.channel(j);
    if (j < n - 1)  jsonString += "},";
    else jsonString += "}";

  }
  jsonString += ("]}");
  WiFi.scanDelete();

  // Connecting to the API to get a location --------------------------------------

  WiFi.begin(WIFI_NAME, WIFI_PASS);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }

  Serial.println("");
  Serial.println("-------- CERT & TIME--------");
  setClock();
  WiFiClientSecure client;
  BearSSL::X509List cert (GlobalSignCA);
  client.setTrustAnchors (&cert);

  Serial.println("");
  Serial.println("-------- API --------");
  Serial.println("https://" + (String) host + ":443");



  if (int s = client.connect(host, 443)) {
    String request = String("POST ") + String(url) + API_KEY;
    request += " HTTP/1.1\r\n";
    request += "Host: " + (String)host + "\r\n";
    request += "User-Agent: ESP8266\r\n";
    request += "Content-Type: application/json\r\n";
    request += "Content-Length: " + String(jsonString.length()) + "\r\n";
    request += "Connection: close\r\n\r\n";
    request += jsonString;
    Serial.println(request);
    client.println(request);
  }

  // Parse reply from API call -------------------------------------------------
  String response;
  while (client.connected() && !client.available()) delay(1);
  while (client.available()) {
    response += client.readString();
  }
  client.stop();

  Serial.println("");
  Serial.println("-------- Response --------+");
  Serial.println(response);
  Serial.println("+-------------------------");

}


void setClock () {

  configTime (3600, 0, "pool.ntp.org", "time.nist.gov");
  time_t ntp_now = time (nullptr);
  while (ntp_now < 8 * 3600 * 2) {
    delay (100);
    ntp_now = time (nullptr);
  }
  setTime(ntp_now);
  struct tm timeinfo;
  gmtime_r (&ntp_now, &timeinfo);

}
